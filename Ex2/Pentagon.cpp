#include "Pentagon.h"

Pentagon::Pentagon(std::string nam, std::string col, double side) : Shape(nam, col)
{
	setSide(side);
}

Pentagon::~Pentagon()
{
}

void Pentagon::setSide(const double side)
{	
	if (side < 0)
	{
		throw shapeException();
	}
	this->_side = side;
}

void Pentagon::draw()
{
	std::cout << std::endl << "Color is " << getColor() << std::endl << "Name is " << getName() << std::endl << "side is " << getSide() << std::endl << "PentagonArea: " << MathUtils::CalPentagonArea(getSide()) << std::endl;;
}

double Pentagon::getSide() const
{
	return _side;
}
