#pragma once
#include "shape.h"
#include "MathUtils.h"
#include "shapeexception.h"

class Hexagon : public Shape
{
public:
	Hexagon(std::string nam, std::string col, double side);
	~Hexagon();
	void setSide(const double side);
	double getSide() const;
	virtual void draw();

private:
	double _side;
};