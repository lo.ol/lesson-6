#pragma once
#include "shape.h"
#include "shapeexception.h"
#include "MathUtils.h"
class Pentagon : public Shape
{
public:
	Pentagon(std::string nam, std::string col, double side);
	~Pentagon();
	void setSide(const double side);
	virtual void draw();
	double getSide() const;
private:
	double _side;
};

