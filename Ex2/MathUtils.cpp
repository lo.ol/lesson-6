#include "MathUtils.h"

MathUtils::MathUtils()
{
}

MathUtils::~MathUtils()
{
}

double MathUtils::CalPentagonArea(double sideLen)
{
	double area = 0;
	
	area = 0.25 * sqrt(5 * (5 + 2 * sqrt(5))) * pow(sideLen,2);

	return area;
}

double MathUtils::CalHexagonArea(double sideLen)
{
	double area = 0;
	
	area = (3 * sqrt(3) / 2) * pow(sideLen, 2);
	return area;
}
