#include "Hexagon.h"

Hexagon::Hexagon(std::string nam, std::string col, double side) : Shape(nam, col)
{
	setSide(side);
}


Hexagon::~Hexagon()
{
}

void Hexagon::setSide(const double side)
{
	if (side < 0)
	{
		throw shapeException();
	}
	this->_side = side;
}

void Hexagon::draw()
{
	std::cout << std::endl << "Color is " << getColor() << std::endl << "Name is " << getName() << std::endl << "side is " << getSide() << std::endl << "PentagonArea: " << MathUtils::CalHexagonArea(getSide()) << std::endl;;
}

double Hexagon::getSide() const
{
	return _side;
}
