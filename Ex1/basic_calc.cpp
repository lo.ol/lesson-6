#include <iostream>
#define ILLEGAL 8200

int add(int a, int b, bool &valid) {
	if (a + b == ILLEGAL)
	{
		std::cout << "This user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year\n";
		valid = false;
		return 0;
    }
	return a + b;
}

int  multiply(int a, int b, bool &valid) {
	int sum = 0;
	
	 for (int i = 0; i < b && valid; i++) {

	  		  sum = add(sum, a, valid);
			  
	 };
  
  
  return sum;
}

int  pow(int a, int b, bool &valid) {
  int exponent = 1;
  
  
	  for (int i = 0; i < b && valid; i++) {
		  
		  exponent = multiply(exponent, a, valid);
	  };
  
  
  return exponent;
}

int main(void) {
	
	bool valid = true;
	int result = pow(820, 100, valid);
	if (valid)
	{
		std::cout << result << std::endl;
	}
	

  system("pause");
}