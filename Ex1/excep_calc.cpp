#include <iostream>
#include <exception>
#define ILLEGAL 8200

int add(int a, int b) {
	if ((a + b) == ILLEGAL)
	{
		throw "This user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year\n";
	}
  return a + b;
}

int  multiply(int a, int b) {
  int sum = 0;
  for(int i = 0; i < b; i++) {
	  
    sum = add(sum, a);
	if (sum == ILLEGAL)
	{
		throw "This user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year\n";
	}
  };
  return sum;
}

int  pow(int a, int b) {
  int exponent = 1;
  for(int i = 0; i < b; i++) {
    exponent = multiply(exponent, a);
	if (exponent == ILLEGAL)
	{
		throw "This user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year\n";
	}
  };
  return exponent;
}

/*int main(void) {
	try {
		std::cout << pow(10, 10) << std::endl;
	} 
	catch (const char* e)
	{
		std::cout << e;
	}
	system("pause");
}*/